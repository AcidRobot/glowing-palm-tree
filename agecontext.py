
import random

"initialize variables"
iLuck = random.randrange(0,12)
'dSkill = {"Test": 11, "Test2": 12}'
iWisdom = 1
iStrength = 1
iHealth = 100
iStamina = 100
sDirection = ""
ranCreature = ""

"functions"
def NEWS(sDirection, iHealth, iStamina, iStrength):
	while sDirection.find('north') == -1  or sDirection.find('east') == -1 or sDirection.find('south') == -1 or sDirection.find('west') == -1:
		sDirection = input()

		"remove from Stamina"
		iStamina = iStamina - 1
		"is Stamina less than 50 then subtract get hungry"
		if iStamina < 50:
			print ("Stamina is", iStamina)
			iHealth = iHealth - 1
		if sDirection.find('north') != -1  or sDirection.find('east') != -1 or sDirection.find('south') != -1 or sDirection.find('west') != -1:
			return sDirection, iStamina, iHealth, iStrength
		else:
			print ("Invalid Option, try again: ")
		if iStamina < 97:
			print ("Valid options: head north, south, east, west")
		pass
	pass

def fight(sDirection, iHealth, iStamina, iStrength, iWisdom, ranCreatureHealth):
	if 10 > iStrength >= 5:
		attack = random.randrange(5,10)
		ranCreatureHealth = ranCreatureHealth - attack
		pass
	elif iStrength < 5:
		attack = random.randrange(1,5)
		ranCreatureHealth = ranCreatureHealth - attack
		pass
	elif 15 > iStrength >= 10:
		attack = random.randrange(9,15)
		ranCreatureHealth = ranCreatureHealth - attack
		pass
	return sDirection, iHealth, iStamina, iStrength, iWisdom, ranCreatureHealth

def Encounter(sDirection, iHealth, iStamina, iStrength, iWisdom):
	lCreature = ['Bulworm', 'goblin', 'elf', 'orc', 'Wraith']
	ranCreature = lCreature[random.randrange(0,4)]
	ranCreatureHealth = (iStrength*random.randrange(1,10)) / 3
	sEncMonster = ""
	print ('\nYou encounter a monster:', ranCreature)
	print ("What do you do?")
	while sEncMonster.find('attack') == -1  or sEncMonster.find('friend') == -1:
		sEncMonster = input()
		if sEncMonster.find('attack') >= 0:
			while sEncMonster.find('attack') >= 0 and ranCreatureHealth > 0:
				print ("You attack the", ranCreature)
				sDirection, iHealth, iStamina, iStrength, iWisdom, ranCreatureHealth = fight(sDirection, iHealth, iStamina, iStrength, iWisdom, ranCreatureHealth)
				if ranCreatureHealth > 0:
					print ("The", ranCreature, "'s health is", ranCreatureHealth)
					print ("Now what do you do?")
					sEncMonster = input()
					pass
				elif ranCreatureHealth <= 0:
					print ("The monster is dead")
				pass
			iStrength += 2
			return sDirection, iStamina, iHealth, iStrength, iWisdom
		elif sEncMonster.find('friend') >= 0:
			print ('You friend the', ranCreature)
			friends = ranCreature
			print (friends)
			iWisdom += 1
			return sDirection, iStamina, iHealth, iStrength, iWisdom
		elif sEncMonster.find('throw rock') >= 0:
			print ("You missed")
			pass
		else:
			print ("Invalid option, try again.")
			pass

		pass
	return sDirection, iStamina, iHealth, iStrength, iWisdom

print ("Hello and Welcome to the new world of odd and morose\nYou have wandered into an open plain and have just now gained conseincnuos\nWhat do you do?")

sDirection, iStamina, iHealth, iStrength = NEWS(sDirection, iHealth, iStamina, iStrength)


print ("\nYou head in the", sDirection,"direction 1 unit")
sDirection, iStamina, iHealth, iStrength, iWisdom = Encounter(sDirection, iHealth, iStamina, iStrength, iWisdom)
for x in xrange(1,5):
	print ("You continue your journey in which direction?")
	sDirection, iStamina, iHealth, iStrength = NEWS(sDirection, iHealth, iStamina, iStrength)

	print ("\nYou", sDirection,"1 unit")
	sDirection, iStamina, iHealth, iStrength, iWisdom = Encounter(sDirection, iHealth, iStamina, iStrength, iWisdom)
	pass


print ('sDirection:', sDirection, '\niStamina:', iStamina, '\niHealth:', iHealth, '\niStrength:', iStrength, '\niWisdom:', iWisdom)

